import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:peliculas/src/models/pelicula_model.dart';

class PeliculasProvider {
  String _apikey = 'd4a57a264cbcc6d9187df7e913200b59';
  String _url = 'api.themoviedb.org';
  String _language = 'es-ES';
  int _page = 0;
  bool _cargando = false;

  List<Pelicula> _populares = List();

  final _popularesStreamController =
      StreamController<List<Pelicula>>.broadcast();

  Function(List<Pelicula>) get popularesSink =>
      _popularesStreamController.sink.add;
  Stream<List<Pelicula>> get populareStream =>
      _popularesStreamController.stream;

  void disposeStreams() {
    _popularesStreamController?.close();
  }

  Future<List<Pelicula>> getEnCines() async {
    Map<String, String> parameter = {'api_key': _apikey, 'language': _language};
    final url = Uri.https(_url, '3/movie/now_playing', parameter);

    final response = await http.get(url);

    final decodedData = json.decode(response.body);

    final peliculas = Peliculas.fromJsonList(decodedData['results']);

    return peliculas.items;
  }

  Future<List<Pelicula>> getPopulates() async {
    if (_cargando) return [];
    _cargando = true;
    _page++;
    Map<String, String> parameter = {
      'api_key': _apikey,
      'language': _language,
      'page': _page.toString()
    };
    final url = Uri.https(_url, '3/movie/popular', parameter);

    final resp = await _procesarRespuesta(url);

    _populares.addAll(resp);
    popularesSink(_populares);
    _cargando = false;
    return resp;
  }

  Future<List<Pelicula>> _procesarRespuesta(url) async {
    final response = await http.get(url);

    final decodedData = json.decode(response.body);

    return Peliculas.fromJsonList(decodedData['results']).items;
  }
}
